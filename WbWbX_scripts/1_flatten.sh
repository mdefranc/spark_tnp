
# flatten all
./tnp_fitter.py flatten muon generalTracks Z Run2016_UL_HIPM  configs/WbWbX_2016.json --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2016_UL_preVFP
./tnp_fitter.py flatten muon generalTracks Z Run2016_UL       configs/WbWbX_2016.json --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2016_UL_postVFP
./tnp_fitter.py flatten muon generalTracks Z Run2017_UL       configs/WbWbX_2017.json --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2017_UL
./tnp_fitter.py flatten muon generalTracks Z Run2018_UL       configs/WbWbX_2018.json --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2018_UL

