
# recover all submitted jobs (hadoop)
./tnp_fitter.py fit muon generalTracks Z Run2016_UL_HIPM  configs/WbWbX_2016.json --recover --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2016_UL_preVFP   -j16
wait
./tnp_fitter.py fit muon generalTracks Z Run2016_UL       configs/WbWbX_2016.json --recover --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2016_UL_postVFP  -j16
wait
./tnp_fitter.py fit muon generalTracks Z Run2017_UL       configs/WbWbX_2017.json --recover --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2017_UL          -j16
wait
./tnp_fitter.py fit muon generalTracks Z Run2018_UL       configs/WbWbX_2018.json --recover --baseDir /eos/user/m/mdefranc/TagAndProbe/Run2018_UL          -j16
wait
